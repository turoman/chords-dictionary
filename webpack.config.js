const path = require('path');
const mode = process.env.NODE_ENV === 'production' ? 'production' : 'development';
const CopyPlugin = require("copy-webpack-plugin");

module.exports = {
    mode: mode,

    entry: './src/app.js',
    output: {
        filename: 'app.js',
        path: path.resolve(__dirname, 'dist')
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            }
        ]
    },

    devtool: 'source-map',

    devServer: {
        contentBase: './dist'
    },

    plugins: [
        new CopyPlugin({
            patterns: [
                { from: "src/index.html", to: "../dist" },
                { from: "src/images", to: "../dist/images" },
                { from: "src/styles.css", to: "../dist" },
                { from: "src/favicon.ico", to: "../dist" },
                { from: "src/data.json", to: "../dist" },
            ],
        }),
    ],
}