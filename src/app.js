/**
 * Global variables
 */
const card = document.querySelector('#chord-panel');
let inputChords = []; // Stores the chords selected by the user
let uiState = []; // Stores the UI state, i.e. which chord inversion is currently shown (e.g. "Am inversion X out of Y")
const minutesToKeep = 1440; // How long to keep chords in local storage
const imgDir = 'images';

/**
 * Let's go 
 */
window.addEventListener('load', Initialize());

/**
 * Adds event listeners.
 */
function Initialize() {
    loadChords();
    if (document.body.contains(document.querySelector('#selectChordRoot'))) {
        document.querySelector('#selectChordRoot').addEventListener('change', function () {
            selectChords();
        });
    }
    if (document.body.contains(document.querySelector('#selectChordType'))) {
        document.querySelector('#selectChordType').addEventListener('change', function () {
            selectChords();
        });
    }
    // If there is a node with id #option-piano, add listener for switching to piano chord view
    if (document.body.contains(document.querySelector('#option-piano'))) {
        document.querySelector('#option-piano').addEventListener('click', function () {
            showPianoChords();
        });
    }
    // If there is a node with id #option-guitar, add listener for switching to guitar chord view
    if (document.body.contains(document.querySelector('#option-guitar'))) {
        // On requesting definitions for guitar
        document.querySelector('#option-guitar').addEventListener('click', function () {
            showGuitarChords();
        });
    }
    // If there is a node with #chord-panel id
    if (document.body.contains(document.querySelector('#chord-panel'))) {
        // On requesting the next definition of this chord
        document.querySelector('#chord-panel').addEventListener('click', function (e) {
            navigateChord(e, 'chord-next', 'next');
            e.preventDefault();
        });
        // On requesting the previous definition of this chord
        document.querySelector('#chord-panel').addEventListener('click', function (e) {
            navigateChord(e, 'chord-prev', 'prev');
            e.preventDefault();
        });
    }
    // If the page has spans with .tab-chord class    
    if (document.querySelectorAll('.tab-chord').length > 0) {
        // Populate the input chords (chords that will have their definitions shown)
        collectInputChordsFromPage(spans);
    };
    // On clicking the "Reset" button
    if (document.body.contains(document.querySelector('.button'))) {
        document.querySelector('.button').addEventListener('click', function (e) {
            e.preventDefault();
            document.location.reload();
        })
    }
}

/**
 * Collects chords that are to be displayed.
 * @param {*} spans The collection of spans that have '.tab-chord' class
 */
function collectInputChordsFromPage(spans) {
    for (i = 0; i < spans.length; i++) {
        chordName = spans[i].innerText;
        if (inputChords.includes(chordName) === false) {
            inputChords.push(chordName);
        }
    }
    loadChords();
}

/**
 * Loads chord definitions into the page.
 */
function loadChords() {
    let chords;
    if (localStorage.getItem('chords') === null) {
        // if no local storage exists, fetch chords from JSON file
        fetchChords(selectChords);
    } else {
        chords = JSON.parse(localStorage.getItem('chords'));
        if (isLocalDataOld(chords[0]['lastUpdatedAt']) === true) {
            // local storage exists but is old, so update it
            fetchChords(selectChords);
        } else {
            // render chords from local storage
            chords = JSON.parse(localStorage.getItem('chords'));
            selectChords();
        }
    }
}

function selectChords() {
    // Clear any previously shown chords
    inputChords.splice(0);
    // Read the currently selected chords
    let listRoot = document.querySelector('#selectChordRoot');
    let listType = document.querySelector('#selectChordType');
    let instrument = document.querySelector('input[name="instrumentRadio"]:checked').value;
    let chordRoot = listRoot.options[listRoot.selectedIndex].value;
    let chordType = listType.options[listType.selectedIndex].value;

    if (chordType == 0) {
        // Iterate through all values in the "Chord Type" list
        for (let index = 0; index < listType.options.length; index++) {
            const chordType = listType.options[index].value;
            if (chordType !== "0") {
                let selectedChordName = composeChordName(chordRoot, chordType);
                inputChords.push(selectedChordName);
            }
        }
    } else {
        let selectedChordName = composeChordName(chordRoot, chordType);
        inputChords.push(selectedChordName);
    }

    // // Read the currently selected instrument
    if (instrument == "guitar") {
        showGuitarChords();
    } else if (instrument == "piano") {
        // If piano is selected        
        showPianoChords();
    } else {
        // Default instrument        
        showGuitarChords();
    }
}

function composeChordName(chordRoot, chordType) {
    // Major chords don't have the type appended to their name by convention
    if (chordType == 'M') {
        return chordRoot + '';
    } else {
        return chordRoot + chordType;
    }
}

/**
 * Handles the event when a next or prev inversion of a chord is clicked
 * @param {*} e The click event
 * @param {*} targetClassName The CSS class name of the clicked link
 * @param {*} direction Navigation direction can be next or prev
 */
function navigateChord(e, targetClassName, direction) {
    if (e.target.className === targetClassName) {
        const chordDefEl = e.target.parentElement.parentElement;
        const chordName = chordDefEl.children[0].innerText;

        // Get the number of inversions for this chord
        let total = getTotalInversions(chordDefEl);

        // Save the state of currently clicked chord and variant to a global array
        updateUIState(chordName, total, direction);

        // Get the 1-based index of the currently clicked inversion
        let current = getInversionIdx(chordName);

        // Update the DOM
        showInversion(chordDefEl, chordName, current, total);
    }
}

/**
 * Returns the inversion currently shown to the user (the X part from "X out of Y")
 * @param {*} chordName 
 */
function getInversionIdx(chordName) {
    for (i = 0; i < uiState.length; i++) {
        if (uiState[i].id === chordName) {
            return uiState[i].current;
        }
    }
}

/**
 * Returns the count of inversions that the given chord has.
 * @param {*} chordDefEl 
 */
function getTotalInversions(chordDefEl) {
    let inversions = 0;
    for (i = 0; i < chordDefEl.children.length; i++) {
        if (chordDefEl.children[i].className === 'chord-variant') {
            inversions++;
        }
    }
    return inversions;
}

/**
 * Saves the state of clicked chords and inversions to the uiState variable.
 * Used to navigate between various inversions of the same chord.
 * @param {*} chordName 
 * @param {*} inversions 
 * @param {*} direction
 */
function updateUIState(chordName, inversions, direction) {
    // If a chord variant is clicked for the first time, push the first chord into uiState
    if (uiState.length === 0) {
        direction === "next" ? current = 2 : current = inversions;
        uiState.push({ id: chordName, current: current, variants: inversions });
    } else {
        // If a chord with this name already exists in uiState
        let uiChord = findUIChord(chordName);
        if (uiChord.length > 0) {
            // Update the state of the existing chord
            updateChordState(chordName, inversions, direction);
        } else {
            // Add this chord to the uiState
            direction === "next" ? current = 2 : current = inversions;
            uiState.push({ id: chordName, current: current, variants: inversions });
        }
    }
}

/**
 * Returns a chord saved previously to the uiState array, given the chord name.
 * @param {*} chordName 
 */
function findUIChord(chordName) {
    return uiState.filter(function () {
        for (i = 0; i < uiState.length; i++) {
            if (uiState[i].id === chordName) {
                return true;
            }
        }
    })
}

/**
 * "Remembers" the current state of a given chord 
 * (which inversion out of total is currently being shown)
 * @param {*} chordName 
 * @param {*} inversions 
 * @param {*} direction
 */
function updateChordState(chordName, inversions, direction) {
    for (i = 0; i < uiState.length; i++) {
        if (uiState[i].id === chordName) {
            let current = uiState[i].current;
            if (direction === "next") {
                current++;
            } else {
                current--;
            }

            if (current > inversions) {
                uiState[i].current = current % inversions;
            } else if (current < 1) {
                uiState[i].current = inversions;
            } else {
                uiState[i].current = current;
            }
        }
    }
}

/**
 * Displays a specific inversion of a chord.
 * @param {*} chordDefEl 
 * @param {*} chordName 
 * @param {*} current 
 * @param {*} total 
 */
function showInversion(chordDefEl, chordName, current, total) {
    // show only the clicked chord inversion and hide the rest
    for (i = 0; i < chordDefEl.children.length; i++) {
        if (chordDefEl.children[i].className === 'chord-variant') {
            if (current === i) {
                chordDefEl.children[i].style.display = 'block';
            } else {
                chordDefEl.children[i].style.display = 'none';
            }
        }
    }

    // update the navigation HTML
    let chordNavEl = chordDefEl.querySelector('.chord-nav');
    chordNavEl.innerHTML = getChordNavHTML(current, total);
}

/**
 * Retrieves the list of all chord definitions from a JSON file 
 * and populates local storage.
 * @param {*} callback The function to call back on success.
 */
function fetchChords(callback) {
    fetch('data.json')
        .then(res => res.json())
        .then(data => {
            const lastUpdatedAt = new Date();
            data[0]["lastUpdatedAt"] = lastUpdatedAt;
            localStorage.setItem('chords', JSON.stringify(data));
            callback(data, showGuitarChords);
        })
        .catch(err => {
            card.innerHTML = `<div style="color: red;">Failed to retrieve chords. ${err}</div>`;
        });
}


/**
 * Returns true if local data is older than now by the number of minutes
 * set in the minutesToKeep global variable.
 * @param {*} strDate 
 */
function isLocalDataOld(strDate) {
    const currTime = new Date();
    const lastUpdate = new Date();
    lastUpdate.setUTCFullYear(parseInt(strDate.substring(0, 4)));
    lastUpdate.setUTCMonth(parseInt(strDate.substring(5, 7)) - 1);
    lastUpdate.setUTCDate(parseInt(strDate.substring(8, 10)));
    lastUpdate.setUTCHours(parseInt(strDate.substring(11, 13)));
    lastUpdate.setUTCMinutes(parseInt(strDate.substring(14, 16)));
    lastUpdate.setUTCSeconds(parseInt(strDate.substring(17, 19)));

    const diff = currTime - lastUpdate; // in milliseconds    
    const diffMinutes = diff / 1000 / 60;
    return diffMinutes > minutesToKeep ? true : false;
}

/** 
 * Iterates through chords originating from browser's local storage and writes their HTML to the DOM. 
 */
function showGuitarChords() {
    let chordHTML = '';
    const chords = JSON.parse(localStorage.getItem('chords'));
    chords.forEach(function (chord) {
        inputChords.forEach(function (name) {
            if (name === chord["name"]) {
                chordHTML += getGuitarChord(chord);
            }
        })
    });
    card.innerHTML = chordHTML;
}

/** 
 * Iterates through chords originating from browser's local storage and writes their HTML to the DOM. 
 */
function showGuitarChordsAccessible() {
    let chordHTML = '';
    let matchedChords = 0; // A counter of how many chords were matched 
    const chords = JSON.parse(localStorage.getItem('chords'));
    chords.forEach(function (chord) {
        inputChords.forEach(function (name) {
            if (name === chord["name"]) {
                chordHTML += describeGuitarChord(chord);
                matchedChords++;
            }
        })
    });

    // If no chords matched the search criteria, inform the user
    if (matchedChords == 0) {
        chordHTML += "<p>Deocamdată, nu există definiții pentru acest acord.</p>"
    }
    card.innerHTML = chordHTML;
}

/** 
 * Iterates through chords originating from browser's local storage and writes their HTML to the DOM. 
 */
function showPianoChords() {
    let chordHTML = '';
    const chords = JSON.parse(localStorage.getItem('chords'));
    chords.forEach(function (chord) {
        inputChords.forEach(function (name) {
            if (name === chord["name"]) {
                chordHTML += getPianoChord(chord);
            }
        });
    });
    card.innerHTML = chordHTML;
}

/**
 * Generates and returns HTML code for a single chord at a time
 * @param {*} chord 
 */
function getGuitarChord(chord) {
    let retval = '';
    retval += `
        <div class="chord-def">
            <div class="chord-title">${chord["name"]}</div>`;

    for (let i = 0; i < chord["guitar"].length; i++) {
        retval += `           
        <div class="chord-variant" style="${hideVariants(i)}">
            <div class="chord-fret">${chord["guitar"][i][0]}</div>
            <table>
                <tr><td>${getXORow(chord["guitar"][i][1])}</td></tr>
                <tr><td>${getLineRow()}</td></tr>
                <tr><td>${getLineRow()}</td></tr>
                <tr><td>${getFretRow(chord["guitar"][i][2])}</td></tr>
                <tr><td>${getLineRow()}</td></tr>
                <tr><td>${getFretRow(chord["guitar"][i][3])}</td></tr>
                <tr><td>${getLineRow()}</td></tr>
                <tr><td>${getFretRow(chord["guitar"][i][4])}</td></tr>
                <tr><td>${getLineRow()}</td></tr>
                <tr><td>${getFretRow(chord["guitar"][i][5])}</td></tr>
                <tr><td>${getLineRow()}</td></tr>
                <tr><td>${getFretRow(chord["guitar"][i][6])}</td></tr>
                <tr><td>${getLineRow()}</td></tr>
            </table>
        </div>`;
    }

    if (chord["guitar"].length > 1) {
        const totalInversions = chord["guitar"].length;
        retval += `<div class="chord-nav">${getChordNavHTML(1, totalInversions)}</div>`;
    } else {
        retval += `<div class="chord-nav">&nbsp;</div>`;
    }

    retval += '</div>';

    return retval;
}

/**
 * Generates and returns HTML code for a single piano chord at a time
 * @param {*} chord 
 */
function getPianoChord(chord) {
    let retval = '';
    retval += `
        <div class="chord-def">
            <div class="chord-title">${chord["name"]}</div>`;

    for (let i = 0; i < chord["piano"].length; i++) {
        retval += `
        <div class="chord-variant" style="${hideVariants(i)}">
            <div class="chord-piano">${getPianoHtml(chord["piano"][i])}</div>
        </div>`;
    }

    retval += '</div>';

    return retval;
}

/**
 * Returns HTML code from the piano chord object supplied as argument.
 * @param {*} obj 
 */
function getPianoHtml(obj) {
    let chordHTML = '';
    const black = obj[0];
    const white = obj[1];
    for (let i = 0; i < 14; i++) {
        if (black[i] === ' ' && white[i] === ' ' && black[i + 1] === ' ' && black[i - 1] === ' ') {
            chordHTML += `<img src="${imgDir}/piano/1.png"/>`;
        } else if (black[i] === ' ' && white[i] === '0' && black[i - 1] === ' ') {
            chordHTML += `<img src="${imgDir}/piano/2.png"/>`;
        } else if (black[i] === '|' && white[i] === ' ' && (black[i - 1] === ' ' || i === 0)) {
            chordHTML += `<img src="${imgDir}/piano/3.png"/>`;
        } else if (black[i] === '|' && white[i] === '0' && (black[i - 1] === ' ' || i === 0)) {
            chordHTML += `<img src="${imgDir}/piano/4.png"/>`;
        } else if (black[i] === '0' && white[i] === ' ' && (black[i - 1] === ' ' || i === 0)) {
            chordHTML += `<img src="${imgDir}/piano/5.png"/>`;
        } else if (black[i] === '0' && white[i] === ' ' && (black[i - 1] === ' ' || i === 0)) {
            chordHTML += `<img src="${imgDir}/piano/6.png"/>`;
        } else if (black[i] === ' ' && white[i] === ' ' && black[i - 1] === '|') {
            chordHTML += `<img src="${imgDir}/piano/11.png"/>`;
        } else if (black[i] === ' ' && white[i] === '0' && black[i - 1] === '|') {
            chordHTML += `<img src="${imgDir}/piano/12.png"/>`;
        } else if (black[i] === '|' && white[i] === ' ' && black[i - 1] === '|') {
            chordHTML += `<img src="${imgDir}/piano/13.png"/>`;
        } else if (black[i] === '|' && white[i] === '0' && black[i - 1] === '|') {
            chordHTML += `<img src="${imgDir}/piano/14.png"/>`;
        } else if (black[i] === '0' && white[i] === ' ' && black[i - 1] === '|') {
            chordHTML += `<img src="${imgDir}/piano/15.png"/>`;
        } else if (black[i] === '0' && white[i] === '0' && black[i - 1] === '|') {
            chordHTML += `<img src="${imgDir}/piano/16.png"/>`;
        } else if (black[i] === ' ' && white[i] === ' ' && black[i - 1] === '0') {
            chordHTML += `<img src="${imgDir}/piano/21.png"/>`;
        } else if (black[i] === ' ' && white[i] === '0' && black[i - 1] === '0') {
            chordHTML += `<img src="${imgDir}/piano/22.png"/>`;
        } else if (black[i] === '|' && white[i] === ' ' && black[i - 1] === '0') {
            chordHTML += `<img src="${imgDir}/piano/23.png"/>`;
        } else if (black[i] === '|' && white[i] === '0' && black[i - 1] === '0') {
            chordHTML += `<img src="${imgDir}/piano/24.png"/>`;
        } else if (black[i] === '0' && white[i] === ' ' && black[i - 1] === '0') {
            chordHTML += `<img src="${imgDir}/piano/25.png"/>`;
        } else if (black[i] === '0' && white[i] === '0' && black[i - 1] === '0') {
            chordHTML += `<img src="${imgDir}/piano/26.png"/>`;
        }
    }

    chordHTML += `<img src="${imgDir}/piano/_.png"/>`;

    return chordHTML;
}


/**
 * Returns the CSS style (hide or show) to be used for a given inversion.
 * @param {*} index 
 */
function hideVariants(index) {
    if (index === 0) {
        return 'display: block;';
    } else {
        return 'display: none';
    }
}

/**
 * Returns HTML code that represents navigation of a chord's inversions.
 * @param {*} current 
 * @param {*} total 
 */
function getChordNavHTML(current, total) {

    return `<a class="chord-prev" href="#">&larr;</a>
            ${current} / ${total}
         <a class="chord-next" href="#">&rarr;</a>`;
}

/**
 * Returns HTML code that displays info about mute/open strings.
 * @param {string} strInfo 
 */
function getXORow(strInfo) {

    let strInfoHTML = '';

    for (let i = 0; i < 6; i++) {
        switch (strInfo[i]) {
            case 'x':
                strInfoHTML += `<img src="${imgDir}/guitar/x.png"/>`;
                break;
            case 'o':
                strInfoHTML += `<img src="${imgDir}/guitar/o.png"/>`;
                break;
            default:
                strInfoHTML += `<img src="${imgDir}/guitar/s.png"/>`;
        }
    }

    return strInfoHTML;
}

/**
 * Returns decorative HTML code that marks the beginning of frets.
 */
function getLineRow() {
    let lineHTML = '';

    for (let i = 0; i < 6; i++) {
        if (i === 0) {
            lineHTML += `<img src="${imgDir}/guitar/l2.png"/>`;
        } else if (i === 5) {
            lineHTML += `<img src="${imgDir}/guitar/2l.png"/>`;
        } else {
            lineHTML += `<img src="${imgDir}/guitar/l.png"/>`;
        }
    }

    return lineHTML;
}

/**
 * Returns HTML code that displays info about one single fret.
 * @param {*} fretInfo 
 */
function getFretRow(fretInfo) {
    let rowHTML = '';

    for (let i = 0; i < 6; i++) {
        if (i === 0) {
            rowHTML += getFirstCell(fretInfo[i], fretInfo[i + 1]);
        } else if (i === 5) {
            rowHTML += getLastCell(fretInfo[i], fretInfo[i - 1]);
        } else {
            rowHTML += getMiddleCell(fretInfo[i], fretInfo[i - 1], fretInfo[i + 1]);
        }
    }

    return rowHTML;
}

/**
 * Returns HTML code that displays info about the first cell of a fret.
 * @param {string} cellInfo Can be "-", "1", "2", "3", "4", "5"
 * @param {string} nextCellInfo Required for barred chords
 */
function getFirstCell(cellInfo, nextCellInfo) {
    let cellHTML = '';
    switch (cellInfo) {
        case '-':
            cellHTML += `<img src="${imgDir}/guitar/0.png"/>`;
            break;
        default:
            if (cellInfo === nextCellInfo) {
                cellHTML += `<img src="${imgDir}/guitar/${cellInfo}_.png"/>`;
            } else {
                cellHTML += `<img src="${imgDir}/guitar/${cellInfo}.png"/>`;
            }
    }

    return cellHTML;
}

/**
 * Returns HTML code that displays info about the last cell of a fret.
 * @param {string} cellInfo Can be "-", "1", "2", "3", "4", "5"
 * @param {string} prevCellInfo Required for barred chords
 */
function getLastCell(cellInfo, prevCellInfo) {
    let cellHTML = '';
    switch (cellInfo) {
        case '-':
            cellHTML += `<img src="${imgDir}/guitar/0.png"/>`;
            break;
        default:
            if (cellInfo === prevCellInfo) {
                cellHTML += `<img src="${imgDir}/guitar/_n.png"/>`;
            } else {
                cellHTML += `<img src="${imgDir}/guitar/${cellInfo}.png"/>`;
            }
    }

    return cellHTML;
}

/**
 * Returns HTML code that displays info about any cell of a fret
 * that is not the first or the last one.
 * @param {string} cellInfo 
 * @param {string} prevCellInfo 
 * @param {string} nextCellInfo 
 */
function getMiddleCell(cellInfo, prevCellInfo, nextCellInfo) {
    let cellHTML = '';
    switch (cellInfo) {
        case '-':
            cellHTML += `<img src="${imgDir}/guitar/0.png"/>`;
            break;
        default:
            if (cellInfo === prevCellInfo && cellInfo === nextCellInfo) {
                cellHTML += `<img src="${imgDir}/guitar/_.png"/>`;
            } else if (cellInfo === prevCellInfo && nextCellInfo === '-') {
                cellHTML += `<img src="${imgDir}/guitar/_n.png"/>`;
            } else if (cellInfo !== nextCellInfo) {
                cellHTML += `<img src="${imgDir}/guitar/${cellInfo}.png"/>`;
            } else {
                cellHTML += `<img src="${imgDir}/guitar/${cellInfo}_.png"/>`;
            }
    }

    return cellHTML;
}

/**
 * Generates text description for a guitar chord for accessibility.
 * @param {*} chord 
 */
function describeGuitarChord(chord) {

    let retval = '';

    retval += `
        <div class="chord-description mt-3">
            <p>Arhiva conține ${chord["guitar"].length} definiții pentru acordul <strong>${chord["name"]}</strong>.<p>`;

    // Iterate through all definitions of a chord to display links to them
    retval += `<ul class="list-unstyled">`;
    for (let i = 0; i < chord["guitar"].length; i++) {
        let definitionLines = chord["guitar"][i];
        let startingFret = convertRomanToInteger(definitionLines[0]);
        let firstRealFret = findFirstRealFret(definitionLines, startingFret);
        if (firstRealFret == 0) {
            // this is an open chord
            retval += `<li><a href="#${chord["name"] + "_" + (i + 1)}">Acordul ${chord["name"]} deschis</a></li>`;
        } else {
            retval += `<li><a href="#${chord["name"] + "_" + (i + 1)}">Acordul ${chord["name"]} începând cu poziția ${firstRealFret}</a></li>`;
        }
    }
    retval += `</ul>`;

    // Iterate again through all definitions of a chord to display their text description
    for (let i = 0; i < chord["guitar"].length; i++) {
        let definitionLines = chord["guitar"][i];
        let startingFret = convertRomanToInteger(definitionLines[0]);
        let firstRealFret = findFirstRealFret(definitionLines, startingFret);
        if (firstRealFret == 0) {
            // this is an open chord
            retval += `<h2 id="${chord["name"] + "_" + (i + 1)}">Acordul ${chord["name"]} deschis</h2>`;
        } else {
            retval += `<h2 id="${chord["name"] + "_" + (i + 1)}">Acordul ${chord["name"]} începând cu poziția ${firstRealFret}</h2>`;
        }

        retval += `<ul>`;

        // Desribe the position of each finger
        retval += describeFingerPosition(1, definitionLines, startingFret);
        retval += describeFingerPosition(2, definitionLines, startingFret);
        retval += describeFingerPosition(3, definitionLines, startingFret);
        retval += describeFingerPosition(4, definitionLines, startingFret);

        // Describe open or closed strings
        let openStrings = definitionLines[1];
        // The iteration is inverse because the 1st item in array is the 6th string on the fret
        for (let o = 5; o >= 0; o--) {
            if (openStrings[o] == "x") {
                retval += `<li>Coarda ${Math.abs(o - 6)} nu trebuie atinsă</li>`;
            }
            if (openStrings[o] == "o") {
                retval += `<li>Coarda ${Math.abs(o - 6)} este ciupită liber</li>`;
            }
        }

        retval += `</ul>`;
        retval += `<div class="mb-3"><a href="#labelSelect">Selectează alt acord</a></div>`;
    }

    retval += '</div>';
    return retval;
}

/**
 * 
 * @param {*} fingerNumber The number of the finger
 * @param {*} definitionLines 
 */
function describeFingerPosition(fingerNumber, definitionLines, startingFret) {
    let retval = '';
    for (let k = 0; k < 7; k++) {
        let fingerFret = definitionLines[k];
        // If this fret contains this finger
        if (fingerFret.indexOf(String(fingerNumber)) >= 0) {
            let firstIx = fingerFret.indexOf(String(fingerNumber)); // first occurrence of this finger
            let lastIx = fingerFret.lastIndexOf(fingerNumber); // last occurrence of this finger
            if (firstIx >= 0) {
                // Calculate the fret number beginning the starting fret for this chord, minus 2,
                // because lines 0 and 1 of each chord def  inition store different info (not relevant to frets)
                let fret = k + startingFret - 2;
                if (firstIx == lastIx) {
                    retval += `<li>Degetul ${fingerNumber} apasă tasta ${fret} pe coarda ${Math.abs(firstIx - 6)}</li>`;
                } else {
                    retval += `<li>Degetul ${fingerNumber} barează tasta ${fret} începând cu coarda ${Math.abs(firstIx - 6)} și până la coarda ${Math.abs(lastIx - 6)}</li>`;
                }
            };
        }
    }

    return retval;
}

/**
 * Returns 0 or the 1-based index of the fret where finger 1 is. Note that this is 
 * not always the same as the fret advertised by the Roman numeral in line 1 of each definition.
 * A return value of 0 means that the chord is open.
 * @param {Array} lines The definition lines of this chord definition.
 * @param {int} startingFret The starting fret that appears in line1 of each definition.
 */
function findFirstRealFret(lines, startingFret) {
    // if the chord has an open string, return 1 as first real fret
    if (lines[1].indexOf("o") >= 0) {
        return 0;
    } else {
        // iterate through all lines and find the one where a barre occurs
        for (let index = 0; index < lines.length; index++) {
            let fingerFret = lines[index];
            // If this fret contains finger 1
            if (fingerFret.indexOf(String("1")) >= 0) {
                let firstIx = fingerFret.indexOf(String("1")); // first occurrence of finger 1
                if (firstIx >= 0) {
                    // Calculate the fret number beginning the starting fret for this chord, minus 2,
                    // because lines 0 and 1 of each chord definition store different info (not relevant to frets)
                    return startingFret - 2 + index;
                };
            }
        }
    }
}

/**
 * Returns the integer representation of a Roman number from I through XII .
 * @param {*} romanNumber 
 */
function convertRomanToInteger(value) {
    switch (value) {
        case "I": return 1;
        case "II": return 2;
        case "III": return 3;
        case "IV": return 4;
        case "V": return 5;
        case "VI": return 6;
        case "VII": return 7;
        case "VIII": return 8;
        case "IX": return 9;
        case "X": return 10;
        case "XI": return 11;
        case "XII": return 12;
    }
}

/**
 * Reads the check boxes of a "Piano Control" div and returns two lines of 14 characters each that represent the definition of a chord for piano.
 * @param {NodeList} blackKeys A NodeList object that contains the set of check boxes defining the black keys (10 items).
 * @param {NodeList} whiteKeys A NodeList object that contains the set of check boxes defining the white keys (14 items).
 */
function getPianoDefinition(blackKeys, whiteKeys) {

    var line1 = [];
    var line2 = [];

    // In each line of black keys, positions 2, 6, 9, 13 are  empty spaces
    // All other positions are read from the selected check boxes.
    line1[0] = convertCheckBox(blackKeys[0]);
    line1[1] = convertCheckBox(blackKeys[1]);
    line1[2] = ' ';
    line1[3] = convertCheckBox(blackKeys[2]);
    line1[4] = convertCheckBox(blackKeys[3]);
    line1[5] = convertCheckBox(blackKeys[4]);
    line1[6] = ' ';
    line1[7] = convertCheckBox(blackKeys[5]);
    line1[8] = convertCheckBox(blackKeys[6]);
    line1[9] = ' ';
    line1[10] = convertCheckBox(blackKeys[7]);
    line1[11] = convertCheckBox(blackKeys[8]);
    line1[12] = convertCheckBox(blackKeys[9]);
    line1[13] = ' ';

    // Create line 2
    for (let index = 0; index < 14; index++) {
        line2[index] = convertCheckBox2(whiteKeys[index]);
    }

    return line1.join("") + "\n" + line2.join("");
}

/**
 * If the check box of a piano key is selected, return "0". Otherwise, return "|".
*/
function convertCheckBox(cbox) {
    return cbox.checked ? '0' : '|';
}

/**
 * If the check box of a piano key is selected, return "0". Otherwise, return " ".
*/
function convertCheckBox2(cbox) {
    return cbox.checked ? '0' : ' ';
}

/**
 * This function is the opposite of getPianoDefiniton(); it populates check boxes of a "Piano Control" from a string received as argument.
 * @param {string} blackCheckBoxName The name of all input check box elements that define black keys.
 * @param {string} whiteCheckBoxName The name of all input check box elements that define white keys.
 * @param {string} textDef The string composed of two lines that define piano keys as text.
 */
function populatePianoControl(blackCheckBoxName, whiteCheckBoxName, lines) {
    // Split the definition into two lines
    var lines = lines.split("\n");
    var blackKeys = lines[0];
    var whiteKeys = lines[1];

    // Populate the black key check boxes. Skip indices 2, 6, 9, and 13 (where there is a gap between black keys)
    document.getElementsByName(blackCheckBoxName)[0].checked = selectCheckBoxPiano(blackKeys[0]);
    document.getElementsByName(blackCheckBoxName)[1].checked = selectCheckBoxPiano(blackKeys[1]);
    document.getElementsByName(blackCheckBoxName)[2].checked = selectCheckBoxPiano(blackKeys[3]);
    document.getElementsByName(blackCheckBoxName)[3].checked = selectCheckBoxPiano(blackKeys[4]);
    document.getElementsByName(blackCheckBoxName)[4].checked = selectCheckBoxPiano(blackKeys[5]);
    document.getElementsByName(blackCheckBoxName)[5].checked = selectCheckBoxPiano(blackKeys[7]);
    document.getElementsByName(blackCheckBoxName)[6].checked = selectCheckBoxPiano(blackKeys[8]);
    document.getElementsByName(blackCheckBoxName)[7].checked = selectCheckBoxPiano(blackKeys[10]);
    document.getElementsByName(blackCheckBoxName)[8].checked = selectCheckBoxPiano(blackKeys[11]);
    document.getElementsByName(blackCheckBoxName)[9].checked = selectCheckBoxPiano(blackKeys[12]);

    for (let index = 0; index < whiteCheckBoxName.length; index++) {
        document.getElementsByName(whiteCheckBoxName)[index].checked = selectCheckBoxPiano(whiteKeys[index]);
    }
}

function selectCheckBoxPiano(char) {
    return char == '0' ? true : false;
}

/**
 * Shows a single guitar definition at a time, from the text supplied as argument
 * @param {string} chordText 
 */
function showGuitarDefinition(chordText) {

    const lines = chordText.split('\n');
    return `<div class="chord-variant">
            <div class="chord-fret">${lines[0]}</div>
            <table class="chord-variant-table">
                <tr><td>${getXORow(lines[1])}</td></tr>
                <tr><td>${getLineRow()}</td></tr>
                <tr><td>${getLineRow()}</td></tr>
                <tr><td>${getFretRow(lines[2])}</td></tr>
                <tr><td>${getLineRow()}</td></tr>
                <tr><td>${getFretRow(lines[3])}</td></tr>
                <tr><td>${getLineRow()}</td></tr>
                <tr><td>${getFretRow(lines[4])}</td></tr>
                <tr><td>${getLineRow()}</td></tr>
                <tr><td>${getFretRow(lines[5])}</td></tr>
                <tr><td>${getLineRow()}</td></tr>
                <tr><td>${getFretRow(lines[6])}</td></tr>
                <tr><td>${getLineRow()}</td></tr>
            </table>
        </div>`;
}

/**
 * Shows a single piano definition at a time, from the text supplied as argument
 * @param {string} chordText 
 */
function showPianoDefinition(chordText) {
    const lines = chordText.split('\n');
    return `<div class="chord-variant">
            <div class="chord-piano">${getPianoHtml(lines)}</div>
        </div>`;
}