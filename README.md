# README

Chords Dictionary is a single-page application (SPA) that displays diagrams for guitar and piano chords.

## Prerequisites

Before you can build and run the app, the following prerequisites must be installed on your computer:

- [node.js](https://nodejs.org/en/)
- [npm](https://www.npmjs.com/)

You can easily check whether these are already installed by running the following commands:

```
npm -v
node -v
```

If the commands above return a version number, then the respective tools are installed.

## Installing dependencies

Once you have installed the prerequisites, open a terminal and change the current directory to the one where you cloned this repository, for example:

```
cd C:\projects\chords-dictionary
```

You can now install dependencies by running the following command in the project's directory:

```
npm install
```

This command creates a **node_modules** directory which stores all of the project's depedencies.

## How to build the app

To build the app for development, run:

```
npm run build-dev
``` 

To build for production, run:

```
npm run build
```

In either case, the output files will be copied to the **dist** directory. The difference between these two builds is as follows:

- When you build for development, the .js files from the **dist** directory are not minified. Also, the development build includes "watch" functionality, meaning that any code change will cause the app to recompile and your change will be immediately available on the development server.
- When you build for production, the .js files are minified. The live reload functionality is disabled in this case.

## How to run the app locally

To start the app on a local development server, run

```
npm start
```

You can now access the app from [http://localhost:8080](http://localhost:8080).

## How to run with Docker

If you have [Docker Desktop](https://www.docker.com/products/docker-desktop) installed, you can build a Docker image for this app and run it in a Docker container, as follows:

1. Build the app as described above.
2. In the project's directory, run:

    ```
    docker-compose up --build
    ```

3. Access [http://localhost:5000](http://localhost:5000) from your browser.

> NOTE: By default, the Docker image is configured to run it on port **5000**. If this port is in use on your computer, edit the `ports` key in **docker-compose.yml** file to specify a different port.
